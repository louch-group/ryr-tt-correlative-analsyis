"""
A simple GUI tool for selecting the XY window to use from a given dataset.

For a given project folder, a single window.h5 file is used to store all
intermediate arrays and flattened images. The selected windows are saved
in {pfolder}/window{n}.txt. If this file already exists, rerunning the
script for the given dataset loads in the old window so that the scripts
simply radjusts the window, instead of creating one from scratch.
"""

import matplotlib.pyplot as plt 
import numpy as np
import os
import glob
import h5py
from skimage import io, filters, morphology
from scipy import ndimage
from matplotlib.widgets import Slider

# Local imports
import util

class WindowSelector:
    """A class for accessing a given imaging dataset and select a XY window.

    To select a window in XY, the RyR and tt z-stacks are first flattened,
    these intermediate flattened images are then stored in the database.
    A 2D image of the RyR and TT images overlaid each other is then showed
    in a GUI where the user can first choose the rotation. After rotation
    is chosen, a different GUI window is used to select the XY window.
    After selection, the window is saved to the h5 database.
    
    Global parameters are
    compr - Sets which compression to use with h5py (default is lzf)
    overwrite - Sets whether the window can be changed. Set to false to avoid
                accidentally changing the window. Note that when the window is
                changed all later analysis **must** be redone.
    ryr_input_filename - The name of the raw input RyR dSTORM files
    blinktable_input_filename - The name of the raw blinktable input files.
    """
    compr = "lzf"
    overwrite = False
    ryr_input_filename = 'ryr'
    tt_input_filename = 'tt'
    

    def __init__(self, pfolder, n, database=None):
        """A project folder and cell number must be specified"""   
        self.f = f"{pfolder}.h5" if database is None else database
        self.pfolder = pfolder
        self.n = n

        # Check if database file exists, and create it if necessary
        if not os.path.isfile(self.f):
            print(f"No database file found. Attempting to create {self.f}")
            db = h5py.File(self.f, 'w')
            db.close()

        # Flatten images for 2D viewing if necessary
        if not self.array_exists("flat_ryr"):
            self.flatten_ryr()
        if not self.array_exists("flat_tt"):
            self.flatten_tt()

        # Check if window already exists and abort/update
        if self.array_exists("window"):
            if self.overwrite:
                self.update_window()
            else:
                raise IOError(f"Window ({self.n}) already exists and"
                               "overwrite is set to False.")
        # Create new window
        else:
            self.select_new_window()
             

    def flatten_ryr(self):
        """Flatted a 3D RyR dSTORM image z-stack by summing over z-axis."""
        ryr = io.imread(f"{self.pfolder}/{self.ryr_input_filename}{self.n}.tif")
        ryr = np.squeeze(ryr)
        flat_ryr = np.sum(ryr, axis=0)
        assert np.isclose(ryr.sum(), flat_ryr.sum())
        assert flat_ryr.ndim == 2
        self.save_array("flat_ryr", flat_ryr)


    def flatten_tt(self):
        """Flatten 3D tt image z-stack by summing odver z-axis."""
        tt = io.imread(f"{self.pfolder}/{self.tt_input_filename}{self.n}.tif")
        tt = np.squeeze(tt)
        flat_tt = np.sum(tt, axis=0)
        assert flat_tt.ndim == 2
        assert np.isclose(tt.sum(), flat_tt.sum())
        self.save_array("flat_tt", flat_tt)


    def select_new_window(self):
        """Select a new window for the given dataset."""
        if self.array_exists("window"):
            raise IOError(f"Window ({self.n} already exists.")
        print(f"Selecting window for: {self.pfolder} - {self.n}", flush=True)
        
        # Set initial values
        angle = 0
        x0 = y0 = 200
        dx = dy = 600
        
        # Run GUI Tools
        angle = self.pick_rotation(angle)
        x0, y0, dx, dy = self.pick_window(angle, (x0, y0, dx, dy))
        
        # Save selected values
        print(f"New Window: {angle:.0f}, {x0:.0f}, {y0:.0f}, {dx:.0f}, {dy:.0f}", flush=True)
        self.save_array("window", (angle, x0, y0, dx, dy))


    def update_window(self):
        """Update an existing window for a given dataset.

        Note that the class attribute overwrite must be set to true.
        If a window is updated, all later analysis **must** be completely 
        redone.
        """
        if not self.array_exists("window"):
            raise IOError(f"Window ({self.n} does not exists, cannot update")
        if not self.overwrite:
            raise IOError(f"Overwrite set to false, cannot update window.") 
        
        # Load existing values
        window = self.load_array("window")        
        angle, x0, y0, dx, dy = window
        print(f"Updating window for: {self.pfolder} - {self.n}", flush=True)
        print(f"Old Window: {angle:.0f}, {x0:.0f}, {y0:.0f}, {dx:.0f}, {dy:.0f}", flush=True)
        
        # Run GUI Tools
        angle = self.pick_rotation(angle)
        x0, y0, dx, dy = self.pick_window(angle, (x0, y0, dx, dy))

        print(f"New Window: {angle:.0f}, {x0:.0f}, {y0:.0f}, {dx:.0f}, {dy:.0f}", flush=True)
        self.save_array("window", (angle, x0, y0, dx, dy))


    def combine_flat_images(self):
        """Overlay flat RyR and tt into correlative 2D image."""
        ryr = self.load_array("flat_ryr").astype(np.int64)
        tt = self.load_array("flat_tt").astype(np.int64)
        ryr = ryr > filters.threshold_otsu(ryr)
        tt = tt/tt.max()
        img = tt + ryr  
        return img
    

    def pick_rotation(self, angle=0):
        """Simple GUI tool to pick rotation."""
        # Overlap the flat RyR and TT images
        img = self.combine_flat_images()
        rotated_image = ndimage.rotate(img, angle)

        # Plot image at initial rotation
        fig, ax = plt.subplots(figsize=(12, 12))
        plt.subplots_adjust(left=0.25, bottom=0.25)
        im = plt.imshow(rotated_image)
        plt.axis('off')
        plt.title("Select rotation (Use slider and/or keyboard). "
                  "Close when done.")

        # Add interactive slider to GUI
        slider_axes = plt.axes([0.25, 0.1, 0.65, 0.03])
        slider = Slider(slider_axes, 'Rotation', -90, 90, valinit=angle, valstep=1.0)
    
        def update(val):
            angle = slider.val
            rotated_image = ndimage.rotate(img, angle)
            im.set_data(rotated_image)
            fig.canvas.draw_idle()
        slider.on_changed(update)

        # Add button functionality to slider
        def on_key(event):
            if event.key == "left":
                slider.set_val(slider.val + 1)
            elif event.key == "right":
                slider.set_val(slider.val - 1)
        cid = fig.canvas.mpl_connect('key_press_event', on_key)
        plt.show()

        # Return selected rotation angle
        return slider.val


    def pick_window(self, rotation, window=(200, 200, 600, 600)):
        """Provide a simple GUI tool to pick the rectangular XY window."""
        # Overlap the flat RyR and TT images
        img = self.combine_flat_images()
        img = ndimage.rotate(img, rotation)

        # Plot image with window rectangle overlaid
        fig, ax = plt.subplots(figsize=(12, 12))
        plt.subplots_adjust(left=0.25, bottom=0.25)
        plt.imshow(img)
        plt.axis('off')
        plt.title("Select xy (Use sliders and/or keyboard. "
                  "Close when done")
        x0, y0, dx, dy = window
        y, = plt.plot((x0, x0+dx, x0+dx, x0, x0), (y0, y0, y0+dy, y0+dy, y0), linewidth=2.0, color='white')
        
        # Add interactive sliders to GUI
        slider_x0 = plt.axes([0.25, 0.1, 0.65, 0.03])
        slider_y0 = plt.axes([0.25, 0.07, 0.65, 0.03])
        slider_dx = plt.axes([0.25, 0.04, 0.65, 0.03])
        slider_dy = plt.axes([0.25, 0.01, 0.65, 0.03])
        slider_x0 = Slider(slider_x0, 'x0', 0, 1200, valinit=x0, valstep=10)
        slider_y0 = Slider(slider_y0, 'y0', 0, 1200, valinit=y0, valstep=10)
        slider_dx = Slider(slider_dx, 'dx', 300, 1650, valinit=dx, valstep=30)
        slider_dy = Slider(slider_dy, 'dy', 300, 1650, valinit=dy, valstep=30)

        def update(val):
            x0 = slider_x0.val
            y0 = slider_y0.val
            dx = slider_dx.val
            dy = slider_dy.val
            y.set_data((x0, x0+dx, x0+dx, x0, x0), (y0, y0, y0+dy, y0+dy, y0))
            fig.canvas.draw_idle()
        slider_x0.on_changed(update)
        slider_y0.on_changed(update)
        slider_dx.on_changed(update)
        slider_dy.on_changed(update)

        # Add buttion functionality to sliders
        def on_key(event):
            if event.key == "left":
                slider_x0.set_val(slider_x0.val - 1)
            elif event.key == "right":
                slider_x0.set_val(slider_x0.val + 1)
            if event.key == "up":
                slider_y0.set_val(slider_y0.val - 1)
            elif event.key == "down":
                slider_y0.set_val(slider_y0.val + 1)
        cid = fig.canvas.mpl_connect('key_press_event', on_key)
        plt.show()

        # Ensure a multiple of 3 voxels is used in each dimension
        x0, y0, dx, dy = slider_x0.val, slider_y0.val, slider_dx.val, slider_dy.val
        dx += (3 - dx % 3)*(dx % 3 != 0)
        dy += (3 - dy % 3)*(dy % 3 != 0)
        assert dx % 3 == 0
        assert dy % 3 == 0
        
        # Return selected XY window
        return x0, y0, dx, dy


    def array_exists(self, name):
        """Check if a given array is stored in the database."""
        with h5py.File(self.f, 'r') as infile:
            return f"{name}_{self.n}" in infile


    def load_array(self, name):
        """Read out a given array from database and return as np.ndarray."""
        with h5py.File(self.f, 'r') as infile:
            if f"{name}_{self.n}" not in infile:
                raise IOError(f"No dataset {name} ({self.n}) in file.")
            data = infile[f"{name}_{self.n}"][()]
        return data


    def save_array(self, name, data):
        """Save a given array to the database under a given name.

        If the given array name is already in use in the database
        there are two possible outcomes. If the class variable overwrite
        is set to False, an IOError is raised, otherwise the old data is
        overwritten and lost.
        """
        with h5py.File(self.f, 'a') as outfile:
            if f"{name}_{self.n}" in outfile:
                if self.overwrite:
                    self.del_array(name)
                else:
                    raise IOError(f"Dataset {name} ({self.n}) already exists and overwrite is set to False.")
            outfile.create_dataset(f"{name}_{self.n}", data=data, compression=self.compr)


    def del_array(self, name):
        """Delete a given array from the database.
        
        Note that for safety, deleting only works if
        the class variable 'overwrite' is set to True.
        """
        with h5py.File(self.f, 'a') as outfile:
            if f"{name}_{self.n}" in outfile:
                if self.overwrite:
                    del outfile[f"{name}_{self.n}"]
                else:
                    raise IOError(f"Cannot delete array while GeometryDatabase.overwrite is set to False.")
            else:
                raise ValueError(f"No dataset {name} ({self.n}) in file, cannot delete.")



def traverse_project(pfolder):
    """Run the Window Select Tool on all cases in a given project folder."""
    if not os.path.isdir(pfolder):
        raise OSError(f"The given project folder ({pfolder}) does not exist."
                        "Please check spelling/placement.")

    target = f"{pfolder}/{WindowSelector.tt_input_filename}*.tif"
    ns = sorted([os.path.basename(f)[2:-4] for f in glob.glob(target)])
    for n in ns:
        try:
            ws = WindowSelector(pfolder, n)
        except IOError:
            print(f"{n}: Window already exists", flush=True)

        

if __name__ == '__main__':
    pfolder = "Sham/14 06 18 (Sham Correlative)"
    traverse_project(pfolder)
