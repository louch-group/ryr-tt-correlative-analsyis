"""
Combine sheets in a given .xlxs datafile.

The results from analyze_geometry.py are written to a 
spreadsheet file (.xlsx), with one tab per cell 
for a given project folder. This script can
go through and merge data from all cells
to give all dyadic/non-dyadic number from 
across all cells in three new tabs:
    - All geom has data from all tabs merged
    - Contains only all dyadic clusters (from All geom)
    - Contains only all non-dyadic clusters (from All geom)
"""

import pandas
import glob
import os
import openpyxl
import numpy as np

def delete_tabs(filename, targets=('All geom', 'Dyadic', 'Nondyadic')):
    """Delete a tab in a given spreadsheet.

    The targets All geom, Dyadic and Nondyadic are the ones
    this script creates. These should be deleted if the script is rerun
    for a given project folder - or you get duplicate tabs.
    """
    workbook = openpyxl.load_workbook(filename)
    names = workbook.get_sheet_names()
    for target in targets:
        if target in names:
            sheet = workbook.get_sheet_by_name(target)
            workbook.remove_sheet(sheet)
    workbook.save(filename)


def merge_cells(filename, threshold=400):
    """Set up new tab by merging data from all cell tabs

    The results spreadsheet has one tab per cell in the 
    project folder. This script merges all the data
    into the new tab 'All geom' (the old tabs still exist).

    In addition, if a threshold argument is given, the tabs
    'Dyadic' and 'Nondyadic' are created based on distance
    to nearest t-tubule centerline (in nm).
    """
    sheets = pandas.read_excel(filename, sheet_name=None)
    for s in sheets:
        cell = int(s[4:])
        n = len(sheets[s])
        sheets[s]['Cell'] = np.ones(n, dtype=np.uint8)*cell

    df = pandas.concat([s for n, s in sheets.items() if n not in ['All geom', 'Dyadic', 'Nondyadic']])

    book = openpyxl.load_workbook(filename)
    writer = pandas.ExcelWriter(filename)
    writer.book = book

    df.to_excel(writer, sheet_name=f"All geom", index=False)

    if threshold is not None:
        near = df[df['Distance'] <= threshold]
        far = df[df['Distance'] > threshold]

        near.to_excel(writer, sheet_name=f"Dyadic", index=False)
        far.to_excel(writer, sheet_name=f"Nondyadic", index=False)        

    writer.save()
    writer.close()


if __name__ == '__main__':
    pfolder = "Sham/14 06 18 (Sham Correlative)"
    spreadsheet = f"{pfolder}.xlsx"

    merge_cells(spreadsheet)

    