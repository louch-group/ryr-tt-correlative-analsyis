"""
Analyze correlative 3D dSTORM imaging of RyR and confocal imaging of NCX/Cav3 to find geometric
cluster data such as cluster size, and distance to nearest t-tubule. 
"""

import numpy as np
from scipy import ndimage, spatial 
from skimage import io, filters, morphology, measure
import h5py
import os
import glob 
import pandas
import openpyxl

# Local imports
import blinktable2tif
import util
import window_select_tool


class GeometryDatabase:
    """
    This class represents a single given geometric dataset from a single correlative
    z-stack of 3D data.

    To store the raw and intermediate data, an h5 file is used as a database. All datasets
    belonging to the same project folder are stored in the same h5 database, and specificed using
    the cell nr. The intermediate images and analysis arrays are stored for future efficiency
    under compression.

    The global parameters are
    compr - Sets which compression to use with h5py. Simple tests showed lzf gave a good
            lossless compression without adding to much read/write delay.
    overwrite - Whether operations are allowed to change internal images already saved,
                defaults to False to avoid accidentally erasing data.
    ryr_input_filename - The name of the raw input RyR dSTORM files
    tt_input_filename - The name of the raw tt input files
    blinktable_input_filename - The name of the raw blinktable input files.
    dz - The height in z to include. RyR data is usually 600 nm wide, but we default to 
         120 voxels (1200 nm) to include more tt above and below the RyR.
    """
    compr = "lzf"
    overwrite = False
    ryr_input_filename = 'ryr'
    tt_input_filename = 'tt'
    blinktable_input_filename = 'i'
    dz = 120


    def __init__(self, pfolder, n, autoconstruct=True):
        """Opens an existing database, or constructs it if it does not exist.
    
        The raw data for a database has to be organized in a very specific manner,
        see the README. To initate a databse, the project folder and cell number 
        must be specified. 

        A single database file is used for any given project folder, i.e., all images 
        belonging to the same project folder goes into the same databse. Note however,
        the GeometryDatabase object belongs to a given cell, the object's view into
        the database thus only represents a subset of the data stored.

        When initating a new database object, we check if an existing .h5 database
        exists and create one if none is found. If the database does not contain
        constructed images, these are set up if autoconstruct is set to True.

        By default, the constructor takes the raw data, thresholds it and skeletonizes
        the tt. The keyword argument 'autoconstruct' can be used to disable these automatic
        steps.
        """
        self.f = f"{pfolder}.h5" # Filename of database file
        self.pfolder = pfolder   # Project Folder
        self.n = n               # Cell nr

        print(f"Opening project folder - {self.pfolder}")

        # Check if database file exists, and create it if necessary
        if not os.path.isfile(self.f):
            print(f"No database file found. Attempting to create {self.f}")
            db = h5py.File(self.f, 'w')
            db.close()

        # The rest of the constructor is autoconstruction of the geometry.
        if not autoconstruct:
            return 

        # Select window if not already done
        if not self.array_exists("window"):
            print("No Window selected. Opening Window Select Tool", flush=True)
            wst = window_select_tool.WindowSelector(self.pfolder, self.n)
            del wst 

        # Extract data windows if not present
        if not self.array_exists("extracted_ryr"):
            print("No extracted RyR window found. Extracting now.", flush=True)
            self.extract_ryr_subdomain()
        if not self.array_exists("extracted_tt"):
            print("No extracted TT window found. Extracting now.", flush=True)
            self.extract_tt_subdomain()

        # Reconstruct blinktable images if not present
        if not self.array_exists("blinktable_img_raw"):
            print("No Blinktable reconstruction found. Reconstructing now.")
            self.construct_blinktable_images()

        # Overlap blinktable image to t-tubules if not done
        if not self.array_exists("blinktable_img"):
            print("No Blinktable overlay found. Optimizing Registration.")
            self.overlap_blinktable()

        # Threshold RyR and TT
        if not self.array_exists("clusters"):
            self.threshold_ryr()
        if not self.array_exists("thresholded_tt"):
            self.threshold_tt()

        # Skeletonize TT
        if not self.array_exists("skeleton"):
            self.skeletonize_tt()

    def array_exists(self, name):
        """Check if a given array is stored in the database."""
        with h5py.File(self.f, 'r') as infile:
            return f"{name}_{self.n}" in infile


    def load_array(self, name):
        """Read out a given array from database and return as np.ndarray."""
        with h5py.File(self.f, 'r') as infile:
            if f"{name}_{self.n}" not in infile:
                raise IOError(f"No dataset {name} ({self.n}) in file.")
            data = infile[f"{name}_{self.n}"][()]
        return data


    def save_array(self, name, data):
        """Save a given array to the database under a given name.

        If the given array name is already in use in the database
        there are two possible outcomes. If the class variable overwrite
        is set to False, an IOError is raised, otherwise the old data is
        overwritten and lost.
        """
        with h5py.File(self.f, 'a') as outfile:
            if f"{name}_{self.n}" in outfile:
                if self.overwrite:
                    self.del_array(name)
                else:
                    raise IOError(f"Dataset {name} ({self.n}) already exists and overwrite is set to False.")
            outfile.create_dataset(f"{name}_{self.n}", data=data, compression=self.compr)


    def del_array(self, name):
        """Delete a given array from the database.
        
        Note that for safety, deleting only works if
        the class variable 'overwrite' is set to True.
        """
        with h5py.File(self.f, 'a') as outfile:
            if f"{name}_{self.n}" in outfile:
                if self.overwrite:
                    del outfile[f"{name}_{self.n}"]
                else:
                    raise IOError(f"Cannot delete array while GeometryDatabase.overwrite is set to False.")
            else:
                raise ValueError(f"No dataset {name} ({self.n}) in file, cannot delete.")

    def read_window(self):
        """Read out the selected rectangular window."""
        window = self.load_array("window")        
        angle, x0, y0, dx, dy = window
        return int(angle), int(x0), int(y0), int(dx), int(dy)

    def extract_ryr_subdomain(self):
        """Extract the RyR window from the full domain.

        First the whole domain is rotated a given angle, then a rectangular 
        window is chosen in xy. In z we aim to include all the RyR dSTORM data,
        which we pad slightly to include enough t-tubule. The window to extract
        is taken from the data generated by the window select tool.
        """
        # Check the raw imaging data is present, and read it in if isn't
        if self.array_exists("raw_ryr"):
            ryr = self.load_array("raw_ryr")
        else:
            print("No raw RyR data found, loading now.")
            ryr = io.imread(pfolder + f"/{self.ryr_input_filename}{self.n}.tif")
            ryr = np.squeeze(ryr)
            assert ryr.ndim == 3
            self.save_array("raw_ryr", ryr)       

        # Pad RyR data in z if needed.
        nz = ryr.shape[0]
        dz = self.dz
        if nz < dz:
            mz = (dz - nz) // 2
            pz = mz + (dz - nz) % 2
            ryr = np.pad(ryr, ((mz, pz), (0, 0), (0, 0)), mode='constant')
        assert ryr.shape[0] == dz, f"z axis mismatch"

        # Rotate and extract
        angle, y0, x0, dy, dx = self.read_window()
        ryr = ndimage.rotate(ryr, angle, axes=(2, 1))
        ryr = ryr[:, x0:x0+dx, y0:y0+dy]
        self.save_array("extracted_ryr", ryr)

    def extract_tt_subdomain(self, pad=False):
        """Extract the TT window from the full domain.

        First the whole domain is rotated a given angle, then a rectangular 
        window is chosen in xy. In z we aim to include more tt data than for 
        RyR. The default is 120 voxels. If not enough tt is available, it can 
        be padded, but this requires the kwarg pad to be set to True, otherwise
        an error is thrown. The window to extract is taken from the data generated
        by the window select tool. If there is more than 120 voxels available
        """
        if self.array_exists("raw_tt"):
            tt = self.load_array("raw_tt")
        else:
            print("No raw TT data found, loading now.")
            tt = io.imread(pfolder + f"/{self.tt_input_filename}{self.n}.tif")
            tt = np.squeeze(tt)
            assert tt.ndim == 3
            self.save_array("raw_tt", tt)

        # Pad tt in z if desired
        nz = tt.shape[0]
        dz = self.dz
        if nz == dz - 1:
            tt = np.pad(tt, ((1, 0), (0, 0), (0, 0)), mode='constant')
        elif nz < dz:
            if pad:
                mz = (dz - nz) // 2
                pz = mz + (dz - nz) % 2
                tt = np.pad(tt, ((mz, pz), (0, 0), (0, 0)), mode='constant')
            else:
                raise IOError(f"TT too narrow in z ({tt.shape[0]}) and pad is set to False.")
        elif nz > dz:
            raise IOError(f"TT larger than dz in z, manual extraction/alignment needed.")
        assert tt.shape[0] == dz, "z axis mismatch"

        # Rotate and extract
        angle, y0, x0, dy, dx = self.read_window()
        tt = ndimage.rotate(tt, angle, axes=(2, 1))
        tt = tt[:, x0:x0+dx, y0:y0+dy]
        self.save_array("extracted_tt", tt)


    def construct_blinktable_images(self, scale_precision=None):
        """Reconstruct a 3D image of RyR from the Zeiss blinktable.

        We reconstruct the blinktable to find the number of events observed
        per cluster.
        """
        x, y, z, p_xy, p_z = blinktable2tif.read_table(f"{self.pfolder}/{self.blinktable_input_filename}{self.n}.txt")
        if scale_precision is not None:
            p_xy *= scale_precision
            p_z *= scale_precision  
        # Construct images
        img, events = blinktable2tif.construct_images(x, y, z, p_xy, p_z,
                                                      r_z=10, r_xy=10)
        
        # Rotate images to match zeiss rotation
        angle, _, _, _, _ = self.read_window()
        img = ndimage.rotate(img, angle, axes=(2, 1))
        events = ndimage.rotate(events, angle, axes=(2, 1))

        # Save images
        self.save_array("blinktable_img_raw", img)
        self.save_array("blinktable_events_raw", events)
        

    def overlap_blinktable(self):
        """Extract window from reconstructed blinktable, aligning it to Zeiss.

        The image reconstructed from the blinktable is of comparable size to the 
        full Zeiss image, however, the exact array sizes do not match. To extract
        the same window so that the blinktable RyR and t-tubule image from 
        zeiss are aligned, we must find the best overlay.

        To do this we first sum over y and z and align only the x-axis. Next
        we do the same with the y-axis. With the xy-plane aligned, we fit the 
        z-axis.
        """
        _, y0, x0, dy, dx = self.read_window()

        zeiss = self.load_array("extracted_ryr")
        img = self.load_array("blinktable_img_raw")
        nz, nx, ny = img.shape

        zeiss = zeiss/zeiss.max()
        img = img/img.max()

        zflat = np.sum(zeiss, axis=0)
        iflat = np.sum(img, axis=0)

        zx = np.sum(zflat, axis=1)
        zy = np.sum(zflat, axis=0)
        ix = np.sum(iflat, axis=1)
        iy = np.sum(iflat, axis=0)

        assert(len(zx) == dx), f"zx: {len(zx)}, dx: {dx}"
        assert(len(zy) == dy), f"zy: {len(zy)}, dy: {dy}"
        assert(len(ix) > dx)
        assert(len(iy) > dy)

        best = 0
        for x in range(nx-dx+1):
            overlap = zx.dot(ix[x:x+dx])
            if overlap > best:
                best = overlap
                bestx = x
        print("X coordinate found.", flush=True)

        best = 0
        for y in range(ny-dy+1):
            overlap = zy.dot(iy[y:y+dy])
            if overlap > best:
                best = overlap
                besty = y
        print("Y coordinate found.", flush=True)

        img = img[:, bestx:bestx+dx, besty:besty+dy]

        dz = zeiss.shape[0] - img.shape[0]
        best = 0
        if dz > 0:
            for z in range(dz+1):
                overlap = zeiss[z:z+nz].ravel().dot(img.ravel())
                if overlap > best:
                    best = overlap
                    bestz = z

        elif dz < 0:
            for z in range(abs(dz)+1):
                overlap = zeiss.ravel().dot(img[z:z+zeiss.shape[0]].ravel())
                if overlap > best:
                    best = overlap
                    bestz = z       
        print("Z coordinate found.", flush=True)


        if dz > 0:
            img = np.pad(img, ((bestz, dz-bestz), (0, 0), (0, 0)), mode='constant')
        elif dz < 0:
            img = img[bestz:bestz+zeiss.shape[0]]
        self.save_array("blinktable_img", img)
        del img

        events = self.load_array("blinktable_events_raw")
        events = events[:, bestx:bestx+dx, besty:besty+dy]
        if dz > 0:
            events = np.pad(events, ((bestz, dz-bestz), (0, 0), (0, 0)), mode='constant')
        elif dz < 0:
            events = events[bestz:bestz+zeiss.shape[0]]
        self.save_array("blinktable_events", events)


    def threshold_ryr(self):
        ryr = self.load_array("blinktable_img")
        ryr = util.downsample_voxels_3(ryr)
        ryr = ryr > filters.threshold_otsu(ryr)
        ryr = util.upsample_voxels_3(ryr, mode='full')
        self.save_array("clusters", ryr)


    def threshold_tt(self, offset=None):
        tt = self.load_array("extracted_tt")
        #tt = tt > filters.threshold_mean(tt)
        if offset is None:
            tt = util.adaptive_threshold(tt)
        else:
            tt = util.adaptive_threshold(tt, offset=offset)
        tt = morphology.remove_small_objects(tt, min_size=1e6)
        self.save_array("thresholded_tt", tt)

    def skeletonize_tt(self):
        tt = self.load_array("thresholded_tt")
        skeleton = morphology.skeletonize_3d(tt) > 0
        self.save_array("skeleton", skeleton)

    def quantify_clusters(self, p_z=7, p_xy=4):
        """Quantify cluster data.

        For each cluster we record the number of events,
        the cluster volume, and the distance to the nearest tt skeleton.

        The p_z and p_xy are precision margins used to find the number of events
        that contribute to a cluster. For example, if p_z is set to 7, an event within
        7 voxels of the thresholded cluster in the z-dimension is counted towards the
        events for that cluster.
        """
        print(f"Quantifying clusters of {self.pfolder} - Geometry {self.n}", flush=True)
        clusters = self.load_array("clusters")
        skeleton = self.load_array("skeleton")
        events = self.load_array("blinktable_events")

        labels, n = measure.label(clusters, background=0, connectivity=3, return_num=True)
        print(f"{n} clusters identified in total.", flush=True)
        print(f"-"*45, flush=True)
        print(f"Progress: ", flush=True, end='')
        milestone = n//35

        # Set up the tt-skeleton tree
        tree = spatial.cKDTree(list(zip(*np.nonzero(skeleton))))

        clusterdata = []
        for i in range(1, n+1):
            if i % milestone == 0:
                print("-", end="", flush=True)
            cluster = (labels == i)
            volume = cluster.sum()

            z, x, y = np.nonzero(cluster)
            z0, z1 = z.min(), z.max()+1
            x0, x1 = x.min(), x.max()+1
            y0, y1 = y.min(), y.max()+1
            nr_events = events[z0-p_z: z1+p_z, x0-p_xy:x1+p_xy, y0-p_xy:y1+p_xy].sum()

            clusterpoints = list(zip(z, x, y))
            dist, ind = tree.query(clusterpoints)
            d = 10*min(dist)
            clusterdata.append([i, volume, nr_events, d])
        print("|", end='\n')
        self.save_to_xlsx(clusterdata)
    
    def save_to_stl(self, dataname, filename=None):
        """Saves the given data to stl using Marching Cubes."""
        data = self.load_array(dataname)
        if filename is None:
            filename = f"stl/{self.pfolder}_{dataname}_{self.n}.stl"
        util.save_to_stl(filename, data)

    def save_to_xlsx(self, data):
        """Save clusterdata to a spreadsheet.

        A spreadsheet is made for each pfolder, with each cell getting
        it's own tab in the sheet. After all cells are made, a separate script
        can be used to combine the data in each tab.
        """
        spreadsheet = f"{self.pfolder}.xlsx"
        if os.path.isfile(spreadsheet):
            book = openpyxl.load_workbook(spreadsheet)
            writer = pandas.ExcelWriter(spreadsheet)
            writer.book = book
        else:
            writer = pandas.ExcelWriter(spreadsheet)

        df = pandas.DataFrame(data)
        df.to_excel(writer, sheet_name=f"geom{self.n}", index=False, 
                    header=["Id", "Volume", "Events", "Distance"])
        writer.save()
        writer.close()


if __name__ == '__main__':
    pfolder = "Sham/14 06 18 (Sham Correlative)"

    for n in sorted(util.find_indices(pfolder)):
        geom = GeometryDatabase(pfolder, n)
        geom.quantify_clusters()
