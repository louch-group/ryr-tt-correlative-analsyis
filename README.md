# README #

This is a collection of scripts used to analyse correlative images of d-STORM RyR imaging data with confocal imaging of t-tubules. If you have any questions, please contact jonas@simula.no.

This README tries to give an overview of what is needed to run the scripts. Note especially that data must be placed in a specific and consistent manner for the scripts to find all they need.

To analyze a dataset, the following pipeline must be used
1. Ensure all data is ordered in the manner required
2. Run window_select_tool.py to select rotation and XY windows for a given project folder
3. Run the analyze_geometry.py script on the same project folder (This will take time)
4. Output is written to a .xlsx file next to the project folder itself.
5. The combine_spreadsheets.py script can now be run to combine tabs across cell numbers for a given project folder

As an example, you can try the project 'Sham/14 06 18 (Sham Correlative)'.

### Dependencies ###

Make sure you run the code in Python 3.6+. The code is incompatible with Python 2. 

Other dependencies are:
- numpy
- scipy
- pandas 
- matplotlib
- h5py
- scikit-image (aka skimage)
- numpy-stl
- openpyxl

All dependencies should be available through pip.

### Input Data Organization ###

These scripts are made to run with a specific datastructure. For a given dataset, the scripts need access to three files:

1. An 3D RyR Image in TIF format
2. An 3D TT image in TIF format
3. An blinktable of RyR events in .txt format

As we typically have several datasets for each class of cells we name each dataset of three files a *case*, all cases that belong together are organized into a *project folder*.

For the scripts to automatically find all the files, a strict naming scheme should be followed. All RyR image files should be named consistently. The scripts assume a default naming scheme of 
* RyR image files - 'ryr{n}.tif'
* TT image files - 'tt{n}.tif' 
* Blinktables - 'i{n}.txt'

In all three cases *n* is the case number. The case numbers within a project folder do not need be contigious, but make sure that all three files are available for each case. Note that the supplied 3D RyR image is produced by the microscope software from the blinktable. If this data is missing, it can be recreated from the blinktable, the ryr tif is thus strictly not necessary, but still assumed to exist by these scripts.

### Intermediate data ###

As we are working with big datasets, a lot of the analysis is quite slow and we therefore store intermediate data to save ourselves from work in the future. All these intermediary files are saved in a .h5 database called {project-folder}.h5, and stored next to the project folder itself. The database will become quite large, and can be deleted after analysis is done to save space if desired. However, ideally you should keep the database to save time in the future and for future reference. Note also that the selected window is stored in the database, and you might want to keep it to make analysis reproducible in the future. The analysis results themselves are saved in a .xlsx spreadsheet file which is named {project-folder}.xlsx and saved next to the projcect data folder itself.


### List of Scripts ###

The following scripts are included

* analyze_geometry.py - Thresholds data, finds centerlines of t-tubules and deems clusters to be dyadic/nondyadic
* blinktable2tif.py - Construct a 3D tif data of RyR from a blinktable. Needed to estimate blinks per cluster
* combine_spreadsheets.py - Combine tabs for different cells into a single tab for easier analysis
* util.py - Various utility functions
* window_select_tool - Basic GUI for selecting a rectangualar subset of the raw data to analyse 

