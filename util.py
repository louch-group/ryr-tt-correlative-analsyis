"""
Module containing various utility functions.
"""

from skimage import measure
import numpy as np
from stl import mesh
from scipy import ndimage
import glob
import os


def downsample_voxels_3(array, clean=True):
    """Downsamples voxels by a factor of 3 by summing.

    Sum 3x3x3 voxels into a single new voxel. The optional
    keyword clean specifies wether the dimensions of the
    input array should cleanly divide into three. If set
    to false, the input array is padded with 0's to make
    this so.
    """
    nz, nx, ny = array.shape
    if not clean:
        if nz % 3 == 2:
            array = np.pad(array, ((1, 0), (0, 0), (0, 0)), mode='constant')
        elif nz % 3 == 1:
            array = np.pad(array, ((1, 1), (0, 0), (0, 0)), mode='constant')
        if nx % 3 == 2:
            array = np.pad(array, ((0, 0), (1, 0), (0, 0)), mode='constant')
        elif nx % 3 == 1:
            array = np.pad(array, ((0, 0), (1, 1), (0, 0)), mode='constant')
        if nz % 3 == 2:
            array = np.pad(array, ((0, 0), (0, 0), (1, 0)), mode='constant')
        elif nz % 3 == 1:
            array = np.pad(array, ((0, 0), (0, 0), (1, 1)), mode='constant')
        
    nz, nx, ny = array.shape
    assert (nz % 3, nx % 3, ny % 3) == (0, 0, 0), (nz, nx, ny)

    new_array = np.zeros((nz//3, nx//3, ny//3))
    for dz in range(3):
        for dx in range(3):
            for dy in range(3):
                new_array += array[dz::3, dx::3, dy::3]

    assert np.isclose(array.sum(), new_array.sum())
    return new_array


def upsample_voxels_3(array, mode='full'):
    """Simple upsamples a voxel array by 3.

    Each voxel in a 3D-array turns into 3x3x3 voxels.
    Two different modes are available:
        'full' - fills all 3x3x3 voxels with the same value
        'center' - fills only the center voxel, the rest will be zero.
    """
    nz, nx, ny = array.shape
    new_array = np.zeros((3*nz, 3*nx, 3*ny), dtype=array.dtype)

    if mode == 'full':
        for dz in range(3):
            for dx in range(3):
                for dy in range(3):
                    new_array[dz::3, dx::3, dy::3] += array
    elif mode == 'center':
        new_array[1::3, 1::3, 1::3] += array
    else:
        raise ValueError("Mode not understood, use 'full' or 'center'.")
    return new_array 


def save_to_stl(outfile, data):
    """Convert a 3D binary array to an stl surface mesh file.

    Conversion is done using the marching cube algorithm, as
    implemented in scikit-image. For saving the stl mesh,
    the numpy-stl package is used.
    """
    verts, faces, normals, values = measure.marching_cubes_lewiner(data, 
                                                  allow_degenerate=False)
    cube = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))
    for i, f in enumerate(faces):
        for j in range(3):
            cube.vectors[i][j] = verts[f[j], :]
    cube.save(outfile)


def binary_dilate(skeleton, selem):
    """3D binary dilation with a given structureded element.

    The input array is unchanged, and the result returned as 
    a new, boolean array.
    """
    nz, nx, ny = skeleton.shape
    sz, sx, sy = selem.shape

    dilated = np.zeros((nz+sz, nx+sx, ny+sz), dtype=np.bool)
    selem = selem.astype(np.bool)

    for z, x, y in zip(*np.nonzero(skeleton)):
        dilated[z:z+sz, x:x+sx, y:y+sy] += selem

    mz = sz//2; pz = sz//2 + sz % 2
    mx = sx//2; px = sx//2 + sx % 2
    my = sy//2; py = sy//2 + sy % 2

    return dilated[mz:-pz, mx:-px, my:-py]


def bbox(self, array):
    """Find the binding box of non-zero data

    For a given array, return the bounding box of
    non-zero data. This is used to find the binding box
    of a given cluster.
    """
    z, x, y = np.nonzero(array)
    z0, z1 = z.min(), z.max()+1
    x0, x1 = x.min(), x.max()+1
    y0, y1 = y.min(), y.max()+1
    return (z0, z1), (x0, x1), (y0, y1)


def adaptive_threshold(data, offset=-100, sigma=50):
    """Take an adaptive binary threshold of a given dataset.

    First a Gaussian blur is made of the entire image, this
    represents a local mean. This blur is then shifted by
    an offset and used as the local threshold.

    The two adjustable parameters is the sigma of the Gaussian blur
    and the offset level. In the limit of a large sigma (and no offset),
    the results will approach that of the global filters.threshold_mean.
    """
    blurred = ndimage.gaussian_filter(data, sigma=sigma)
    return data > (blurred - offset)


def find_indices(pfolder):
    """Find cell numbers within a given project folder."""
    files = glob.glob(f"{pfolder}/i*.txt")
    if len(files) > 0:
        return sorted([int(os.path.basename(f)[1:-4]) for f in files])
    else:
        files = glob.glob(f"{pfolder}/window*.txt")
        return sorted([int(os.path.basename(f)[6:-4]) for f in files])
